import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('site_image', t => {
    t.renameColumn('cover_image', 'is_cover_image')
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('site_image', t => {
    t.renameColumn('is_cover_image', 'cover_image')
  })
}

