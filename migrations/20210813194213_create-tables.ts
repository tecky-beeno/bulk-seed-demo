import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('user', t => {
    t.increments('id')
    t.string('username', 32)
    t.string('password', 60)
    t.text('introduction')
  })
  await knex.schema.createTable('site', t => {
    t.increments('id')
    t.integer('owner_id').references('user.id')
    t.text('title',)
  })
  await knex.schema.createTable('site_image', t => {
    t.increments('id')
    t.integer('site_id').references('site.id')
    t.string('image')
    t.boolean('cover_image').defaultTo(false)
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('site_image')
  await knex.schema.dropTable('site')
  await knex.schema.dropTable('user')
}

