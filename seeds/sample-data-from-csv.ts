import * as Knex from 'knex'
import {csv_to_json, from_csv} from '@beenotung/tslib/csv'
import {join} from 'path'
import {readFileSync} from "fs";

namespace CSV {
  export type User = {
    id: string
    username: string
    password: string
    introduction: string
  }

  export type Site = {
    id: string
    owner_id: string
    title: string
  }

  export type SiteImage = {
    id: string
    site_id: string
    image: string
    cover_image?: string
  }
}

namespace Mem {
  export type User = {
    id: string
    db_id?: number
    username: string
    password: string
    introduction: string
  }

  export type Site = {
    id: string
    db_id?: number
    owner: User
    title: string
  }

  export type SiteImage = {
    id: string
    site: Site
    image: string
    is_cover_image: boolean
  }
}

function loadCSV<T>(file: string): T[] {
  file = join('res', 'sample', file)
  let text = readFileSync(file).toString()
  let csv = from_csv(text)
  let json = csv_to_json(csv)
  return json as any
}

export async function seed(knex: Knex): Promise<void> {

  await knex.transaction(async knex => {
    // Deletes ALL existing entries
    await knex('site_image').del()
    await knex('site').del()
    await knex('user').del()

    // link up the records
    let users: Mem.User[] = []
    let sites: Mem.Site[] = []
    let images: Mem.SiteImage[] = []

    for (let user of loadCSV<CSV.User>('users.csv')) {
      users.push(user)
    }

    for (let site of loadCSV<CSV.Site>('site.csv')) {
      let owner = users.find(user => user.id === site.owner_id)
      if (!owner) {
        console.log('failed to find owner:', site);
        continue
      }
      sites.push({
        id: site.id,
        title: site.title,
        owner
      })
    }

    for (let image of loadCSV<CSV.SiteImage>('site_image.csv')) {
      let site = sites.find(site => site.id === image.site_id)
      if (!site) {
        console.log('failed to find site:', site)
        continue
      }
      images.push({
        id: image.id,
        image: image.image,
        is_cover_image: !!image.cover_image,
        site
      })
    }


    // Inserts seed entries
    for (let user of users) {
      const [id] = await knex.insert({
        username: user.username,
        password: user.password,
        introduction: user.introduction
      }).into('user').returning('id')
      user.db_id = id as any
    }

    for (let site of sites) {
      const [id] = await knex.insert({
        owner_id: site.owner.db_id,
        title: site.title
      }).into('site').returning('id')
      site.db_id = id as any
    }

    for (let image of images) {
      await knex.insert({
        image: image.image,
        is_cover_image: image.is_cover_image,
        site_id: image.site.db_id
      }).into('site_image')
    }

  })
}
