import * as Knex from 'knex'
import {hashPassword} from '../hash'

type User = {
  username: string
  password: string
}

type Site = {
  // owner: User
  // imageList: Image[]
  owner_id: number
  size: number
  has_television: boolean
}

type SiteImage = {
  file: string
  site_id: number
}

export async function seed(knex: Knex): Promise<void> {
  await knex.transaction(async knex => {
    // Deletes ALL existing entries
    await knex('image').del()
    await knex('site').del()
    await knex('user').del()

    const alice: User = {
      username: 'alice',
      password: await hashPassword('alice secret'),
    }
    const [alice_id] = await knex.insert(alice).into('user').returning('id')

    const bob: User = {
      username: 'bob',
      password: await hashPassword('bob secret')
    }
    const [bob_id] = await knex.insert(bob).into('user').returning('id')

    const site_1: Site = {
      owner_id: alice_id,
      size: 12,
      has_television: true,
    }
    const [site_1_id] = await knex.insert(site_1).into('site').returning('id')
    const image_1: SiteImage = {
      file: 'image1.jpg',
      site_id: site_1_id
    }
    await knex.insert(image_1).into('site_image')

    // Inserts seed entries
    await knex('table_name').insert([
      {id: 1, colName: 'rowValue1'},
      {id: 2, colName: 'rowValue2'},
      {id: 3, colName: 'rowValue3'},
    ])
  })
}
