import { hash } from 'bcrypt'

const SALT_ROUND = 12

export function hashPassword(plaintext: string): Promise<string> {
  return hash(plaintext, SALT_ROUND)
}
