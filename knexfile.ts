// Update with your config settings.

const configs = {
  development: {
    client: 'sqlite3',
    connection: {
      filename:'dev.db',
      useNullAsDefault: true
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
}

module.exports = configs
export default configs
